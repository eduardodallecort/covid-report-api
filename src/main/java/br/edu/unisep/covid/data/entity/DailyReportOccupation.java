package br.edu.unisep.covid.data.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "daily_report_occupation")
public class DailyReportOccupation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_reportoccupation")
    private Integer id;

    @OneToOne
    @JoinColumn(name = "id_occupation")
    private Occupation occupation;

    @ManyToOne
    @JoinColumn(name = "id_report")
    private DailyReport report;

    @Column(name = "amount")
    private Integer amount;

}
